`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:10:51 05/01/2015 
// Design Name: 
// Module Name:    vgatop 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module vgatop(
		clk,
		//7 seg out
		an,
		seg,
		//vga out
		hs, vs,
		red,
		green,
		blue,
		Led
    );

input clk;
reg clk_d;

output [3:0] an;
output [6:0] seg;

output  hs;
output  vs;
output  Led;

output wire [2:0] red;
output wire [2:0] green;
output wire [1:0] blue;

wire [10:0] hc;
wire [10:0] vc;

reg ven;
assign red = (ven)?0:0;
assign green = (ven)?0:0;
assign blue = (ven)?3:0;

always @(posedge clk)
begin
	clk_d <= ~clk_d;
end

vgargb vga0
(
	.pixel_clk(clk_d),
	.hs(hs),
	.vs(vs),
   .hcount(hc),
	.vcount(vc)
);

seg7display seg70
(
	.ssclk(clk),
	.count(an),
	.mode(seg),
	.led(Led)
);

always @(posedge clk_d) begin
	if (hc >= 0 && hc <= (639) &&
			vc >= 0 && vc <= (479))
	begin
			ven = 1'b1;
	end else begin
			ven = 1'b0;
	end
end
 
endmodule
