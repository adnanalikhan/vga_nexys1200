//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:11:45 05/01/2015 
// Design Name: 
// Module Name:    vga 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module vgargb(
	pixel_clk,
	hs,
	vs,
	hcount,
	vcount
);

input pixel_clk;
output reg	hs;
output reg	vs;
output [10:0] hcount;
output [10:0] vcount;

reg [10:0]	hcounter;
reg [10:0]	vcounter;


assign hcount = hcounter;
assign vcount = vcounter;

initial
begin
		hs = 0;
		vs = 0;
		hcounter = 0;
		vcounter = 0;
end 

always @(posedge pixel_clk)
begin

	if(hcounter < 799)
		hcounter <= hcounter + 1;
	else
	begin
		hcounter <= 0;
		vcounter <= (vcounter < 524)?(vcounter + 1): 0;
	end
	
		
	hs <= ((hcounter >= 648)&&(hcounter < 744))?0:1;
	vs <= ((vcounter >= 482)&&(vcounter < 484))?0:1;	
	
end

endmodule
